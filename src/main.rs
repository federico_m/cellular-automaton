use rand::prelude::*;
use std::{thread, time};

#[derive(Copy, Clone, Debug)]
struct Cell {
    old: bool,
    new: bool,
}

const ROWS: usize = 60;
const COLUMNS: usize = 60;

fn main() {
    let sleep_time = time::Duration::from_millis(200);
    let mut count = 0;
    let mut board = [Cell {
        old: false,
        new: false,
    }; ROWS * COLUMNS];

    let mut rng = rand::thread_rng();

    //init cells
    for c in board.iter_mut() {
        if rng.gen::<f32>() > 0.85 {
            c.old = true;
        }
    }

    //board[9 * ROWS + 10].old = true;
    //board[10 * ROWS + 9].old = true;
    //board[11 * ROWS + 10].old = true;

    print!("\x1b[2J");

    loop {
        print!("\x1b[0;0H");
        println!("Gen: {}", count);
        count += 1;
        print(&board);

        for i in 0..ROWS {
            for j in 0..COLUMNS {
                let neigh = count_neighbours(&board, i, j);

                if board[i * ROWS + j].old {
                    //alive
                    if neigh < 2 {
                        board[i * ROWS + j].new = false;
                    }
                    if neigh == 2 || neigh == 3 {
                        board[i * ROWS + j].new = true;
                    }
                    if neigh > 3 {
                        board[i * ROWS + j].new = false;
                    }
                } else {
                    //dead
                    if neigh == 3 {
                        board[i * ROWS + j].new = true;
                    }
                }
            }
        }
        update(&mut board);
        thread::sleep(sleep_time);
    }
}

fn print(arr: &[Cell; ROWS * COLUMNS]) {
    for i in 0..ROWS {
        for j in 0..COLUMNS {
            print!(
                "{}",
                if arr[i * ROWS + j].old {
                    //"\u{25A0}"
                    "o"
                } else {
                    " "
                }
            );
        }
        println!();
    }
}

fn count_neighbours(arr: &[Cell; ROWS * COLUMNS], x: usize, y: usize) -> u8 {
    let mut res = 0;

    if x >= 1 && y >= 1 && arr[(x - 1) * ROWS + y - 1].old {
        res += 1;
    }
    if y >= 1 && arr[x * ROWS + y - 1].old {
        res += 1;
    }
    if x <= ROWS - 2 && y >= 1 && arr[(x + 1) * ROWS + y - 1].old {
        res += 1;
    }
    if x >= 1 && arr[(x - 1) * ROWS + y].old {
        res += 1;
    }
    if x <= ROWS - 2 && arr[(x + 1) * ROWS + y].old {
        res += 1;
    }
    if x >= 1 && y <= COLUMNS - 2 && arr[(x - 1) * ROWS + y + 1].old {
        res += 1;
    }
    if y <= COLUMNS - 2 && arr[x * ROWS + y + 1].old {
        res += 1;
    }
    if x <= ROWS - 2 && y <= COLUMNS - 2 && arr[(x + 1) * ROWS + y + 1].old {
        res += 1;
    }
    res
}

fn update(arr: &mut [Cell; ROWS * COLUMNS]) {
    for c in arr.iter_mut() {
        c.old = c.new;
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn count_1() {
        let mut board = [Cell {
            old: false,
            new: false,
        }; ROWS * COLUMNS];

        board[5 * ROWS + 5].old = true;
        board[6 * ROWS + 5].old = true;
        board[7 * ROWS + 5].old = true;

        assert_eq!(count_neighbours(&board, 5, 5), 1);
        assert_eq!(count_neighbours(&board, 6, 5), 2);
        assert_eq!(count_neighbours(&board, 7, 5), 1);
    }

    #[test]
    fn count_2() {
        let mut board = [Cell {
            old: false,
            new: false,
        }; ROWS * COLUMNS];

        board[0 * ROWS + 5].old = true;
        board[1 * ROWS + 5].old = true;
        board[2 * ROWS + 5].old = true;

        assert_eq!(count_neighbours(&board, 0, 5), 1);
        assert_eq!(count_neighbours(&board, 1, 5), 2);
        assert_eq!(count_neighbours(&board, 2, 5), 1);
    }

    #[test]
    fn count_3() {
        let mut board = [Cell {
            old: false,
            new: false,
        }; ROWS * COLUMNS];

        board[5 * ROWS + 0].old = true;
        board[5 * ROWS + 1].old = true;
        board[5 * ROWS + 2].old = true;

        assert_eq!(count_neighbours(&board, 5, 0), 1);
        assert_eq!(count_neighbours(&board, 5, 1), 2);
        assert_eq!(count_neighbours(&board, 5, 2), 1);
    }

    #[test]
    fn count_4() {
        let mut board = [Cell {
            old: false,
            new: false,
        }; ROWS * COLUMNS];

        board[(ROWS - 1) * ROWS + 5].old = true;
        board[(ROWS - 2) * ROWS + 5].old = true;
        board[(ROWS - 3) * ROWS + 5].old = true;

        assert_eq!(count_neighbours(&board, ROWS - 1, 5), 1);
        assert_eq!(count_neighbours(&board, ROWS - 2, 5), 2);
        assert_eq!(count_neighbours(&board, ROWS - 3, 5), 1);
    }

    #[test]
    fn count_5() {
        let mut board = [Cell {
            old: false,
            new: false,
        }; ROWS * COLUMNS];

        board[5 * ROWS + COLUMNS - 1].old = true;
        board[5 * ROWS + COLUMNS - 2].old = true;
        board[5 * ROWS + COLUMNS - 3].old = true;

        assert_eq!(count_neighbours(&board, 5, COLUMNS - 1), 1);
        assert_eq!(count_neighbours(&board, 5, COLUMNS - 2), 2);
        assert_eq!(count_neighbours(&board, 5, COLUMNS - 3), 1);
    }

    #[test]
    fn count_6() {
        let mut board = [Cell {
            old: false,
            new: false,
        }; ROWS * COLUMNS];

        board[5 * ROWS + 2].old = true;
        board[6 * ROWS + 1].old = true;
        board[7 * ROWS + 2].old = true;

        board[10 * ROWS + 10].old = true;
        board[9 * ROWS + 11].old = true;
        board[10 * ROWS + 12].old = true;

        assert_eq!(count_neighbours(&board, 6, 2), 3);
        assert_eq!(count_neighbours(&board, 10, 11), 3);
        assert_eq!(count_neighbours(&board, 5, 1), 2);
    }
}
